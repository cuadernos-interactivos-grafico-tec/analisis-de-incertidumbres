# Análisis de incertidumbres

El cuaderno que se presenta desarrolla un ejemplo de estimación de la incertidumbre por medio del método de Monte Carlo para un mismo problema, pero con distintos algoritmos siguiendo el suplemento de la Guía para la estimación de la incertidumbre.

- **TeoremadelLímiteCentral.ipynb** contiene un código en R para comprobar el cumplimiento del teorema del Límite Central.
- **Propagación de Distribuciones por Montecarlo.ipynb** contiene el cuaderno con un resumen del algoritmo, las funciones más comunes para utilizar y ejemplos aplicados a la densidad del aire con distintos abordajes.
- **Magnitudes Correlacionadas.ipynb** contiene la comparación entre el uso de un muestreo multivariado que toma en cuenta las correlaciones y uno que no.
- **Ejercicio de Mínimos Cuadrados.ipynb** contiene un ejercicio de Mínimos cuadrados que utiliza el método de Monte Carlo para mejorar la estimación de los coeficientes.
- **Ejercicios Varios.ipynb** y **Ejercicios Varios2.ipynb** contienen algunos ejercicios resueltos con el método de Monte Carlo.
