{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a5d872bd",
   "metadata": {},
   "source": [
    "# Introducción\n",
    "\n",
    "El siguiente cuaderno busca realizar estimaciones de la incertidumbre por medio de método de Monte Carlo (*MCM*). Esto se realiza siguiendo la recomendación **JCGM 101:2008 \"Evaluation of measurement data — Supplement 1 to the “Guide to the expression of uncertainty in measurement” — Propagation of distributions using a Monte Carlo method\"**\n",
    "\n",
    "Todo lo relacionado con términos, definiciones, convenciones, notación y referencias normativas se puede consultar en los primeros cuatro capítulos de la JCGM 101:2008, este cuaderno es un acompañamiento a partir del capítulo cinco.\n",
    "\n",
    "# Pasos de la evaluación de la incertidumbre\n",
    "\n",
    "1. Formulación: los casos que se trataarán requieren de escribir $Y$, el mensurando, como una función de todas las $X_{i}$ que se desean tomar en cuenta. De forma que se tenga un modelo claro y a cada una de las $X_{i}$ se les debe asignar una función de distribución de probabilidad (*PDF*).\n",
    "2. Propagación: a través del modelo, se propagan todas las *PDF* para obtener la *PDF* de $Y$.\n",
    "3. Simplificación: se reportan los resultados, un valor asociado a $Y$, su valor esperado, con una desviación estándar y el correspondiente factor de cobertura.\n",
    "\n",
    "## Construyendo la *PDF* de $Y$\n",
    "\n",
    "Para poder propagar las distintas *PDF's* hasta $Y$, se aplica el *MCM*. La JCGM 101:2008 propone este algorimo:\n",
    "\n",
    "1. Seleccionar el número $M$ de ensayos de Monte Carlo.\n",
    "2. Generar $M$ vectores de cada una de as N cantidades $X_{i}$.\n",
    "3. Para cada vector, calcular el valor de $Y$, obteniendo un total de $M$ $Y$'s.\n",
    "4. Acomodar los valores en $Y$ en orden creciente, para poder estimar $G$.\n",
    "5. Usar $G$ para obtener el estimado $y$ y la incertidumbre est'andar $u(y)$.\n",
    "6. Usar $G$ para determinar el factor de covertura para $Y$ con una probabilidad $p$ escigida.\n",
    "\n",
    "\n",
    "![algoritmo](figuras/algoritmoMCM.PNG)\n",
    "\n",
    "1. $f$ es continuo para todos los $X_{i}$ en las cercanías de cada estimado $x_{i}$.\n",
    "2. $Y$ tiene una función de distribución continua y estrictamente creciente.\n",
    "3. La *PDF* de $Y$ es estrictamente positiva, unimodal. Además, creciente a la izquierda del modo y decreciente a la derecha de esta.\n",
    "4. Existe un valor esperado $E(Y)$ y un varianza $V(Y)$.\n",
    "5. $M$ es lo suficientemente grande.\n",
    "\n",
    "Este método no requiere de estimar las derivadas parciales de $Y=f(X)$, puesto que depende del muestreo que se haga sobre cada función de distribución. En general no será necesario clasificar las fuentes de incertidumbre en Tipo A o Tipo B; sin embargo, para efectos de este cuaderno, se realizará para poderlo asociar un proceso de medición más real.\n",
    "\n",
    "# *PDF's* para las variables de entrada\n",
    "\n",
    "Se debe tomar en cuenta si las cantidades $X_{i}$ son independientes o dependientes entre sí. Esto determinará la forma en la que se deberá hacer el análisis para que cada *PDF* se asigne según se determine. Se pueden asignar algunas *PDF's* conjuntas si existen variables que no son independientes.\n",
    "\n",
    "Para los casos iniciales, no se analizarán sistemas que requieran de distribuciones de probabilidad conjuntas, una vez que se ahonde más en el teorema de Bayes se podrán explorar otro tipo de modelos.\n",
    "\n",
    "Además, inicialmente no se trabajará con variables que no se hayan medido con anterioridad o que sean desconocidas, esto requiere de trabajar con el principio de Entropía Máxima, y se odrá realizar más adelante.\n",
    "\n",
    "## *PDF's* comunes\n",
    "\n",
    "### Rectangular/Uniforme\n",
    "\n",
    "Si sólo se conocen los límites superior $b$ e inferior $a$, con $a<b$, se asignaría una distribución rectangular en el intervalo $[a,b]$.\n",
    "\n",
    "<div class=\"math\">\n",
    "\\begin{equation}\n",
    "    g_{X}(\\xi)\n",
    "    =\n",
    "    \\left\\{\n",
    "        \\begin{array}{ll}\n",
    "            1/(b-a),\\,& a\\leq\\xi\\leq b,\\\\\n",
    "            0, & \\xi< a\\textrm{ o }b< \\xi.\n",
    "        \\end{array}\n",
    "    \\right.\n",
    "\\end{equation}\n",
    "</div>\n",
    "\n",
    "Para este caso, se tiene que:\n",
    "\n",
    "$$E(X)=\\frac{a+b}{2},\\quad V(X)=\\frac{(b-a)^2}{12}$$\n",
    "\n",
    "Para hacer los muestreos en distribuciones rectangulares se utilizará ```numpy.random.uniform```, que permite generar números aleatorios siguiendo una distribución rectangular entre los números $a$ y $b$.\n",
    "\n",
    "### Trapezoidal\n",
    "\n",
    "En la figura siguiente, se observa la forma que tiene la distribución trapezoidal.\n",
    "\n",
    "![algoritmo](figuras/trapezoidal.PNG)\n",
    "\n",
    "Tomando:\n",
    "\n",
    "$$a=a_{1}+a_{2},\\quad b=b_{1}+b_{2},\\quad \\beta=\\frac{\\lambda_{1}}{\\lambda_{2}},\\quad x=\\frac{a+b}{2}$$\n",
    "\n",
    "Donde:\n",
    "\n",
    "$$\\lambda_{1}=\\frac{[(b_{1}-a_{1})-(b_{2}-a_{2})]}{2},\\quad \\lambda_{2}=\\frac{b-a}{2}$$\n",
    "\n",
    "Se puede escribir la *PDF* como:\n",
    "\n",
    "<div class=\"math\">\n",
    "\\begin{equation}\n",
    "    g_{X}(\\xi)\n",
    "    =\n",
    "    \\left\\{\n",
    "        \\begin{array}{ll}\n",
    "            (\\xi-x+\\lambda_{2})/(\\lambda_{2}^2-\\lambda_{1}^2), & x-\\lambda_{2}\\leq \\xi < x-\\lambda_{1}, \\\\\n",
    "            1/(\\lambda_{1}+\\lambda_{2}), & x-\\lambda_{1}\\leq\\xi\\leq x+\\lambda_{1}, \\\\\n",
    "            (x+\\lambda_{2}-\\xi)/(\\lambda_{2}^2-\\lambda_{1}^2), & x+\\lambda_{1}<\\xi\\leq x+\\lambda_{2}, \\\\\n",
    "            0, & \\textrm{de otra forma}.\n",
    "        \\end{array}\n",
    "    \\right.\n",
    "\\end{equation}\n",
    "</div>\n",
    "\n",
    "Esta distribución tiene un valor esperado y una varianza:\n",
    "\n",
    "$$E(X)=\\frac{a+b}{2},\\quad V(X)=\\frac{(b-a)^2}{24}(1+\\beta^2)$$\n",
    "\n",
    "Para hacer un muestreo de una distribución trapezoidal será necesario generar dos valores de una distribución uniforme en $[0,1]$, de forma que:\n",
    "\n",
    "$$\\xi=a+\\frac{b-a}{2}\\left[(1+\\beta)r_{1}+(1-\\beta)r_{2}\\right]$$\n",
    "\n",
    "### Triangular\n",
    "\n",
    "Conociendo la moda $c$, que se encuentra en $[a,b]$, se puede seleccionar una distribución triangular; sin embargo, esta es por lo general desaconsejada por la *GUM*. Para generar números aleatorios usando esta distribución se puede utilizar ```numpy.random.triangular``` para la cual sólo es necesario digitar los valores de $a$, $b$ y $c$.\n",
    "\n",
    "Para esta distribución, se tiene que:\n",
    "\n",
    "$$E(X)=\\frac{a+b+c}{3},\\quad V(X)=\\frac{a^2+b^2+c^2-ab-ac-bc}{18}$$\n",
    "\n",
    "### t-Student\n",
    "\n",
    "Usnado ```numpy.random.standard_t``` se pueden generar números aleatorios de una distribución t-Student con un n'umero conocido de grados de libertad. En este caso los valores obtenidos vendrán de la distribución estándar, con:\n",
    "\n",
    "$$E(X)=0,\\quad V(X)=\\frac{\\nu}{\\nu-2}$$\n",
    "\n",
    "Con $\\nu>2$. Se puede entonces desplazar la distribución sumando la media deseada.\n",
    "\n",
    "Para simular una incertidumbre Tipo A, es necesario plantear que:\n",
    "\n",
    "$$\\xi=\\bar{x}+\\frac{s}{\\sqrt{n}}t$$\n",
    "\n",
    "Donde $t$ proviene de una distribución estándar t-Student con $n-1$ grados de liberdad.\n",
    "\n",
    "Si como fuente se desea usar un certificado de calibración, es conveniente utilizar una distribución t-Student, que utilice los grados efectivos de libertad $\\nu_{eff}$, el valor reportado y una varianza $(U/k)^2$.\n",
    "\n",
    "### Gaussiana/Normal\n",
    "\n",
    "Usando ```numpy.random.normal``` se pueden generar números aleatorios a partir de una distribución Gaussiana, con una media y desviación estándar conocidas.\n",
    "\n",
    "### Otras funciones\n",
    "\n",
    "Dependiendo de la información que se tenga del problema, así como de las variables que se van a incluir para determinar la *PDF* de $Y$, será necesario utilizar distribuciones como la Gaussiana, la Gaussiana multivariada, la U, la exponencial o la Gamma. De momento no se tomarán estas en cuenta."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "dd8747d2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Todas las librerías han sido importadas.\n"
     ]
    }
   ],
   "source": [
    "# se realizan las importaciones necesarios\n",
    "import numpy as np\n",
    "from numpy.random import standard_t\n",
    "from numpy.random import normal\n",
    "from numpy.random import uniform\n",
    "import matplotlib.pyplot as plt\n",
    "print(\"Todas las librerías han sido importadas.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b5c519b",
   "metadata": {},
   "source": [
    "# Ejemplo 1: Propagación de distribuciones simple\n",
    "\n",
    "A continuación, se simulará un proceso de medición que ya se realizó: densidad de aire, para unas condiciones ya conocidas de temperatura, presión y humedad relativa. La idea es poder obtener la estimación de la incertidumbre por este medio para evaluar el funcionamiento del algorimo. Se incluyen $10$ muestreos en cada variabl; sin embargo, en la práctica se suele tomar una muestra al inicio y una al final, pero para poder experimentar con algoritmo de mejor manera, se tomó un muestreo mayor.\n",
    "\n",
    "## Número de ensayos\n",
    "\n",
    "De acuerdo con la JCGM 101:2008, el número $M$ de ensayos a realizar deb ser al menos $10^4$ veces mayor que el valor de $1/(1-p)$, de forma que si se desea un factor de covertura al $95\\%$ será necesario hacer al menos $0.2\\times10^{6}$ ensayos, que redondeando hacia el número mayor podría pensarse en $10^6$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "09dcd855",
   "metadata": {},
   "outputs": [],
   "source": [
    "M=10**6"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "377849ed",
   "metadata": {},
   "source": [
    "## Variables de entrada"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "6af221f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# parámetros de las variables de entrada\n",
    "## Presión\n",
    "P=858.6 # hPa\n",
    "### Resolución\n",
    "res_p=0.1\n",
    "br_p=res_p/2\n",
    "### EMP\n",
    "EMP_p=0.25\n",
    "be_p=EMP_p\n",
    "### Deriva\n",
    "D_p=EMP_p/3\n",
    "bd_p=D_p\n",
    "### Variación\n",
    "V_P=0.2\n",
    "bv_p=np.abs(V_P)\n",
    "### Muestreo\n",
    "a_p=standard_t(9,size=M)*0.01/np.sqrt(10)+P"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "834b1d06",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Humedad relativa\n",
    "HR=0.688 # hPa\n",
    "### Resolución\n",
    "res_hr=0.001/100\n",
    "br_hr=res_hr/2\n",
    "### EMP\n",
    "EMP_hr=0.01\n",
    "be_hr=EMP_hr\n",
    "### Deriva\n",
    "D_hr=EMP_hr/3\n",
    "bd_hr=D_hr\n",
    "### Variación\n",
    "V_HR=-0.009\n",
    "bv_hr=np.abs(V_HR)\n",
    "### Muestreo\n",
    "a_hr=standard_t(9,size=M)*0.001/np.sqrt(10)+HR"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "7c256390",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Temperatura\n",
    "T=25.3 # hPa\n",
    "### Resolución\n",
    "res_t=0.1\n",
    "br_t=res_t/2\n",
    "### EMP\n",
    "EMP_t=0.2\n",
    "be_t=EMP_t\n",
    "### Deriva\n",
    "D_t=EMP_t/3\n",
    "bd_t=D_t\n",
    "### Variación\n",
    "V_T=0.2\n",
    "bv_t=np.abs(V_T)\n",
    "### Muestreo\n",
    "a_t=standard_t(9,size=M)*0.01/np.sqrt(10)+T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "81e73ffb",
   "metadata": {},
   "outputs": [],
   "source": [
    "Presión=uniform(-br_p,br_p,size=M)+uniform(-be_p,be_p,size=M)+uniform(-bd_p,bd_p,size=M)+uniform(-bv_p,bv_p,size=M)+a_p"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "af9bccaa",
   "metadata": {},
   "outputs": [],
   "source": [
    "Temperatura=uniform(-br_t,br_t,size=M)+uniform(-be_t,be_t,size=M)+uniform(-bd_t,bd_t,size=M)+uniform(-bv_t,bv_t,size=M)+a_t"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "e8b5fc85",
   "metadata": {},
   "outputs": [],
   "source": [
    "HumedadR=uniform(-br_hr,br_hr,size=M)+uniform(-be_hr,be_hr,size=M)+uniform(-bd_hr,bd_hr,size=M)+uniform(-bv_hr,bv_hr,size=M)+a_hr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "69fd6eaf",
   "metadata": {},
   "outputs": [],
   "source": [
    "def Densidad(P,T,HR):\n",
    "    return (0.348444*P+HR*(0.00252*T-0.020582))/(T+273.15)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "22645243",
   "metadata": {},
   "outputs": [],
   "source": [
    "G_rho=Densidad(Presión,Temperatura,HumedadR).tolist()\n",
    "G_rho.sort()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "f908b703",
   "metadata": {},
   "outputs": [],
   "source": [
    "G_rho=np.array(G_rho)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "7116de09",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXcAAAD4CAYAAAAXUaZHAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAASBUlEQVR4nO3df6xfd13H8eeLFqaiZFvWztoWW7CZbCaM5aZiSIgyxxoh64wuqUFsdKYxGYqJBjtJNMbUjJioqEzSAFoj0DQoWcNkslQJMWEbd7DBuq1pZWO9tqwXCKKSjGy8/eOeynftvb3fe+/3fH+c7/OR3JxzPudzvvdzPvd7Xt/P/Xx/paqQJHXLS0bdAEnS4BnuktRBhrskdZDhLkkdZLhLUgetH3UDAK666qratm3bqJshSRPl4Ycf/lpVbVhs31iE+7Zt25idnR11MyRpoiT5ylL7nJaRpA4y3CWpgwx3Seogw12SOshwl6QOMtwlqYMMd0nqIMNdkjrIcJekDjLcNfW27b+3rzJpkhjuktRBhrskdZDhLkkdZLhLUgcZ7pLUQYa7hK+OUfcY7pLUQYa7dAFH8eoCw11awrb99xr0mliGu6aS70pV1xnu0hr4gKBxZbhLK2SgaxL0Fe5JLk/ysSRPJnkiyU8luTLJ/UlONssreurfmeRUkhNJbm6v+dKlXSqIF5tT77e+Aa9x1+/I/b3AfVX148BrgSeA/cCxqtoBHGu2SXItsAe4DtgF3J1k3aAbLvXLINY0Wjbck7wCeCPwQYCq+k5VfRPYDRxqqh0Cbm3WdwOHq+q5qnoKOAXsHGyzpZXpDfiVhr0PDppE/YzcXwXMA3+b5AtJPpDk5cDVVXUWoFlubOpvBk73HD/XlL1Ikn1JZpPMzs/Pr+kkpH60GdI+AGjc9BPu64EbgL+pqtcB/0szBbOELFJWFxVUHayqmaqa2bBhQ1+NlST1p59wnwPmqurBZvtjLIT9s0k2ATTLcz31t/YcvwU4M5jmSmuz2hH2ck+0LraURmnZcK+qrwKnk1zTFN0IPA4cBfY2ZXuBe5r1o8CeJJcl2Q7sAB4aaKulMWCIa5z1+2qZ3wQ+nOSLwPXAnwB3ATclOQnc1GxTVceBIyw8ANwH3FFVLwy43dJF1vKkqdQ16/upVFWPADOL7LpxifoHgAOrb5a0dga8ppnvUJVa4AOLRs1wl6QOMtwlqYMMd3XOuEyJjEs7NJ0Md3WCQSq9mOGuThl1yI/690vnGe6S1EGGuyR1kOEutcjPm9GoGO7SEBnyGhbDXRNv3ANz3NunbjLcNZEmMTAnsc2aXIa7JpqBKS3OcNfE6cKTlJPcdk0Gw10TpQuh2IVz0Pgz3CWpgwx3aUQcwatNhrskdZDhLkkdZLhLUgcZ7poYzlFL/TPcNREMdmllDHdphHzQUlv6CvckTyf5UpJHksw2ZVcmuT/JyWZ5RU/9O5OcSnIiyc1tNV7dNy3hNy3nqeFZycj9Z6rq+qqaabb3A8eqagdwrNkmybXAHuA6YBdwd5J1A2yzJGkZa5mW2Q0catYPAbf2lB+uqueq6ingFLBzDb9HkrRC/YZ7AZ9K8nCSfU3Z1VV1FqBZbmzKNwOne46da8peJMm+JLNJZufn51fXeknSotb3We8NVXUmyUbg/iRPXqJuFimriwqqDgIHAWZmZi7aL0lavb5G7lV1plmeAz7OwjTLs0k2ATTLc031OWBrz+FbgDODarCmx7Q9yTht56t2LRvuSV6e5IfOrwNvBh4DjgJ7m2p7gXua9aPAniSXJdkO7AAeGnTDpa7oDfVt++815DUQ/UzLXA18PMn5+h+pqvuSfA44kuR24BngNoCqOp7kCPA48DxwR1W90Err1VkGnLQ2y4Z7VX0ZeO0i5V8HblzimAPAgTW3TsKgl1bDd6hKUgcZ7hoLjs6lwTLcNXYMemntDHdpDPkAp7Uy3KUxZcBrLQx3jRUD7cXsD62W4S5JHWS4a2w4SpUGx3DXyBnq0uAZ7pLUQYa7JHWQ4S5JHWS4S2PO5yS0Goa7NGEMe/XDcJcmgIGulTLcNVKGltQOw12SOshw18g4al85+0z9MtwlqYMMdw3V+ZGnI9CVs8+0Eoa7hs6QGgz7UZdiuEtSB/Ud7knWJflCkk8021cmuT/JyWZ5RU/dO5OcSnIiyc1tNFyStLSVjNzfCTzRs70fOFZVO4BjzTZJrgX2ANcBu4C7k6wbTHMlSf3oK9yTbAHeAnygp3g3cKhZPwTc2lN+uKqeq6qngFPAzoG0VhLgfLuW1+/I/S+AdwHf7Sm7uqrOAjTLjU35ZuB0T725pkySNCTLhnuStwLnqurhPm8zi5TVIre7L8lsktn5+fk+b1qS1I9+Ru5vAG5J8jRwGHhTkn8Ank2yCaBZnmvqzwFbe47fApy58Ear6mBVzVTVzIYNG9ZwCpoUTiVIw7NsuFfVnVW1paq2sfBE6b9W1S8DR4G9TbW9wD3N+lFgT5LLkmwHdgAPDbzlmigGezt8U5iWsn4Nx94FHElyO/AMcBtAVR1PcgR4HHgeuKOqXlhzSyVJfVvRm5iq6tNV9dZm/etVdWNV7WiW3+ipd6CqXl1V11TVJwfdaE0OR5TSaPgOVbXOgJeGz3BXKwz04bPP1ctw18AZMsNlf2sxhrskdZDhLkkdZLirNU4XSKNjuEsd4gOqzjPcpQ4y5GW4S1IHGe6S1EGGuwbK6YDR828gMNwlqZMMd0nqIMNd6iinZ6ab4a6BMUyk8WG4S1IHGe5atW377/Vr3qQxZbhLUgcZ7lozR+3jzb/PdDLcpSlgwE8fw12rYlhMBv9O08twl6aEQT9dDHdJ6qBlwz3J9yV5KMmjSY4n+aOm/Mok9yc52Syv6DnmziSnkpxIcnObJyBJulg/I/fngDdV1WuB64FdSV4P7AeOVdUO4FizTZJrgT3AdcAu4O4k61pouyRpCcuGey34n2bzpc1PAbuBQ035IeDWZn03cLiqnquqp4BTwM5BNlqSdGnr+6nUjLwfBn4MeF9VPZjk6qo6C1BVZ5NsbKpvBh7oOXyuKbvwNvcB+wBe+cpXrv4MNFQ+KSdNhr6eUK2qF6rqemALsDPJT1yieha7iUVu82BVzVTVzIYNG/pqrKS16f3ICHXbil4tU1XfBD7Nwlz6s0k2ATTLc021OWBrz2FbgDNrbagkqX/9vFpmQ5LLm/XvB34WeBI4Cuxtqu0F7mnWjwJ7klyWZDuwA3howO2WJF1CP3Pum4BDzbz7S4AjVfWJJJ8FjiS5HXgGuA2gqo4nOQI8DjwP3FFVL7TTfEnSYpYN96r6IvC6Rcq/Dty4xDEHgANrbp0kaVV8h6r65hNx3eHfsvsMd0nqIMNdfXGk1z3+TbvNcJekDjLcJamDDHdpijk1012Gu5ZlAEiTx3CXppyfN9NNhrsuyYtemkyGuyR1kOEuSR1kuEsCnILrGsNdS/JilyaX4a5FGezTyb97dxjukl7EgO8Gw12SOshwl6QOMtwlqYMMd72I861SNxjuuogBL+8Dk89w1//zgpa6w3CXpA4y3CUtyf/mJtey4Z5ka5J/S/JEkuNJ3tmUX5nk/iQnm+UVPcfcmeRUkhNJbm7zBDQYXsRSt/Qzcn8e+J2qeg3weuCOJNcC+4FjVbUDONZs0+zbA1wH7ALuTrKujcZLao8P+JNt2XCvqrNV9flm/b+BJ4DNwG7gUFPtEHBrs74bOFxVz1XVU8ApYOeA2y1JuoQVzbkn2Qa8DngQuLqqzsLCAwCwsam2GTjdc9hcU3bhbe1LMptkdn5+fhVNlyQtZX2/FZP8IPCPwG9X1beSLFl1kbK6qKDqIHAQYGZm5qL9Gg7/9Za6qa+Re5KXshDsH66qf2qKn02yqdm/CTjXlM8BW3sO3wKcGUxzJUn96OfVMgE+CDxRVX/Ws+sosLdZ3wvc01O+J8llSbYDO4CHBtdkDYqjdqm7+pmWeQPwduBLSR5pyn4fuAs4kuR24BngNoCqOp7kCPA4C6+0uaOqXhh0wyUNx/lBwNN3vWXELdFKLBvuVfXvLD6PDnDjEsccAA6soV1qmaN2rca2/fca8hPCd6hK6osDgsliuEtSBxnuklbEEfxkMNynkBen1H2GuyR1kOEuacW27b/X/wDHnOEuSR1kuEtSBxnuU8R/ozVo3qfGl+E+ZbwYpenQ90f+anIZ6NL0ceQuSR1kuEtaE/8zHE+Ge8d54WkYvJ+NH8Nd0kAY8OPFcO8wLzZpehnuktRBhrukgfG/xfHh69w7xotLo+ZX8Y0HR+6SBs5BxugZ7h3hxSSpl+HeIQa8pPMMd0mtcLAxWsuGe5IPJTmX5LGesiuT3J/kZLO8omffnUlOJTmR5Oa2Gq7v8SKSdKF+Ru5/B+y6oGw/cKyqdgDHmm2SXAvsAa5rjrk7ybqBtVaS1Jdlw72qPgN844Li3cChZv0QcGtP+eGqeq6qngJOATsH01RdyBG7xp3ftTo6q51zv7qqzgI0y41N+WbgdE+9uabsIkn2JZlNMjs/P7/KZsgLR5PA++nwDfoJ1SxSVotVrKqDVTVTVTMbNmwYcDMkabqtNtyfTbIJoFmea8rngK099bYAZ1bfPEld4eh9uFYb7keBvc36XuCenvI9SS5Lsh3YATy0tiZKklaqn5dCfhT4LHBNkrkktwN3ATclOQnc1GxTVceBI8DjwH3AHVX1QluNn2aOgjSJvN8Oz7IfHFZVv7TErhuXqH8AOLCWRmlpfiiTpH74DlVJQ+XofTgM9wnkxSFpOYa7pKFzgNI+v6xjQngxSFoJR+6SRqL3owkcvAye4S5JHeS0zJhzRKOu8z7eDkfuktRBhvsYc0QjabUM9zFlsGvaeJ8fLMNd0tjwyz0Gx3AfM965JUfxg2C4SxpLDnTWxnAfE96RpcV5XayO4S5p7BnwK2e4jwHvuNLyvE5WxnAfMe+wUv+8Xvrnxw+MiHdSSW1y5D5kPnEqrY3XT38M9yHyTikNhtfS8pyWacH5L7HuXUoarMWuK788/nscubfELyGQhs/r7XsM9wEwyKXx4fW4IFXVzg0nu4D3AuuAD1TVXUvVnZmZqdnZ2Vba0aZpv/NIk6Zr0zZJHq6qmcX2tTLnnmQd8D7gJmAO+FySo1X1eBu/b5gMdGlyXXj9di3se7X1hOpO4FRVfRkgyWFgNzB24X7+j33+j+yToNL0WOo6782D3u1J0sq0TJJfBHZV1a83228HfrKq3tFTZx+wr9m8Bjgx8IaMp6uAr426EWPGPrmYfXIx++RiP1pVGxbb0dbIPYuUvehRpKoOAgdb+v1jK8nsUnNk08o+uZh9cjH7ZGXaerXMHLC1Z3sLcKal3yVJukBb4f45YEeS7UleBuwBjrb0uyRJF2hlWqaqnk/yDuBfWHgp5Ieq6ngbv2sCTd1UVB/sk4vZJxezT1agtde5S5JGx3eoSlIHGe6S1EGG+xok+VCSc0keW2J/kvxlklNJvpjkhp59u5KcaPbt7ym/LcnxJN9NMnEv+2qpT/40yZNN/Y8nuXwIpzIwLfXJHzd1H0nyqSQ/MoxzGZQ2+qRn/+8mqSRXtXkOY6+q/FnlD/BG4AbgsSX2/xzwSRZe9/964MGmfB3wH8CrgJcBjwLXNvtew8Kbuj4NzIz6HMekT94MrG/W3wO8Z9TnOQZ98oqe438LeP+oz3PUfdLs38rCCzm+Alw16vMc5Y8j9zWoqs8A37hEld3A39eCB4DLk2yi5+MZquo7wPmPZ6CqnqiqiX23bkt98qmqer45/gEW3jcxMVrqk2/1HP9yLniT4Lhro08afw68iwnrjzYY7u3aDJzu2Z5rypYqnwZr7ZNfY2FE1yWr6pMkB5KcBt4G/MEQ2jlMK+6TJLcA/1lVjw6rkePMcG/XUh/DsOzHM3TYqvskybuB54EPt9CuUVpVn1TVu6tqKwv98Y5F6k6yFfVJkh8A3k33HuRWzXBv11IfwzDNH8+wqj5Jshd4K/C2aiZXO2St95OPAL/QWutGY6V98mpgO/Bokqeb8s8n+eGhtHYMGe7tOgr8SvPM/+uB/6qqs0z3xzOsuE+aL375PeCWqvr2qBreotX0yY6e428Bnhx2o1u2oj6pqi9V1caq2lZV21h4ELihqr46sjMYMb8gew2SfBT4aeCqJHPAHwIvBaiq9wP/zMKz/qeAbwO/2uxb8uMZkvw88FfABuDeJI9U1c3DPK+1aKNPgL8GLgPuTwLwQFX9xrDOaa1a6pO7klwDfJeFV4ZMTH9Aa32iHn78gCR1kNMyktRBhrskdZDhLkkdZLhLUgcZ7pLUQYa7JHWQ4S5JHfR/N1d50V/i/KcAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.hist(G_rho,bins=1000,density=True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "5213d32c",
   "metadata": {},
   "outputs": [],
   "source": [
    "rho_a=G_rho.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "22fb6c66",
   "metadata": {},
   "outputs": [],
   "source": [
    "u_rho=G_rho.std() # np.sqrt(((G_rho-rho_a)**2).sum()/(M-1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "3a8f9675",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1.0025258555919268, 0.0006137200880337773)"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rho_a,u_rho"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "6ff91e08",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "25000.0"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(M-0.95*M)/2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27edad18-3414-48cd-b37f-98bbad95715f",
   "metadata": {},
   "source": [
    "# Ejemplo 2: Muestreo multivariado\n",
    "\n",
    "Para este caso, se le dará continuidad al problema anterior. En el caso anterior, se tomaron los instrumentos como si midieran por seprado, por eso tenían cada uno su distribución por separado. Sin embargo, esto no siempre es cierto. Para el caso de un barotermohigrómetro, que reliza mediciones de las tres cantidades al mismo tiempo, es necesario incluir correlaciones, así como una distribución de probabilidad conjunta.\n",
    "\n",
    "## Construyendo una distribución de probabilidad conjunta\n",
    "\n",
    "La recomendación recomendación JCGM 101:2008 únicamente habla del uso de una distribución Gaussiana multivariada, y explica la forma correcta para realizar el muestreo, para efectos de Python, se puede utilizar la función ```numpy.random.multivariate_normal``` Es necesario conocer el vector de medias y la matriz de covarianzas propuesta, esta es muy importante, pues se requiere para poder establecer las correlaciones más adelante.\n",
    "\n",
    "No existe un histórico de datos para construir la matriz de covarianza; sin embargo, los valore asignados a cada variable provienen de un promedio que se tomó entre las mediciones inicil y final, por lo que en este caso se tomarán ambos datos para poder construir la matriz de covarianzas que se calculará utilizando ```numpy.cov```."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "861a0f08-d784-4684-9a26-f52b51ea4528",
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy.random import multivariate_normal"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "4f0b13ae-a24a-42f4-b3a0-9f695bf41e38",
   "metadata": {},
   "outputs": [],
   "source": [
    "# matriz de datos\n",
    "A=np.array([[858.7,858.5],\n",
    "            [25.4,25.2],\n",
    "           [0.683,0.692]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "bdfe6aab-ae8c-4349-ba64-1f8de1588d2a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 2.00e-02,  2.00e-02, -9.00e-04],\n",
       "       [ 2.00e-02,  2.00e-02, -9.00e-04],\n",
       "       [-9.00e-04, -9.00e-04,  4.05e-05]])"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# matriz de covarianzas\n",
    "np.cov(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d73b7aa-0adb-4313-908e-4d7743b6700c",
   "metadata": {},
   "source": [
    "Se observan entonces covarianzas positivas para temperatura y presión, y negativas todas las que incluyen a la humedad relativa."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "4383bf1c-8836-4db8-b306-40d957cfa8f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "MuestreoMultiVariado=multivariate_normal(np.array([P,T,HR]),np.cov(A),size=M)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "1e7bf9e8-7898-46bf-830a-0e9ec1243b95",
   "metadata": {},
   "outputs": [],
   "source": [
    "Presión=MuestreoMultiVariado[:,0]+uniform(-br_p,br_p,size=M)+uniform(-be_p,be_p,size=M)+uniform(-bd_p,bd_p,size=M)+uniform(-bv_p,bv_p,size=M)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "38ddee95-e51a-4c5b-a6ee-e4f4968d87be",
   "metadata": {},
   "outputs": [],
   "source": [
    "Temperatura=MuestreoMultiVariado[:,1]+uniform(-br_t,br_t,size=M)+uniform(-be_t,be_t,size=M)+uniform(-bd_t,bd_t,size=M)+uniform(-bv_t,bv_t,size=M)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "f62f99bb-beb6-47b8-8a0e-f131c2e725c5",
   "metadata": {},
   "outputs": [],
   "source": [
    "HumedadR=MuestreoMultiVariado[:,2]+uniform(-br_hr,br_hr,size=M)+uniform(-be_hr,be_hr,size=M)+uniform(-bd_hr,bd_hr,size=M)+uniform(-bv_hr,bv_hr,size=M)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "13d0a1bd-672c-49e6-a3df-812378b8ef25",
   "metadata": {},
   "outputs": [],
   "source": [
    "G_rho=Densidad(Presión,Temperatura,HumedadR).tolist()\n",
    "G_rho.sort()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "cdef54d6-5149-444c-a97b-db376e7c7c14",
   "metadata": {},
   "outputs": [],
   "source": [
    "G_rho=np.array(G_rho)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "71049490-5aeb-4ce1-a90c-c82791567089",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXcAAAD4CAYAAAAXUaZHAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAASm0lEQVR4nO3df4wcZ33H8fenDoSWFpE0duraVh2QRUkq8UOnlAoJqU1DLFLFQRWSEW2tNpKFFFoqFVGnSBVVZcm06k+poXKB1m2ByIKiuEQNWC4IVSqEC4Qfzg/sEoMPG/soopRWCk349o+bwMbeu9vb27ndnX2/JGtmn5m5+z7e2c8+++zsXqoKSVK3/NC4C5AkjZ7hLkkdZLhLUgcZ7pLUQYa7JHXQFeMuAOCaa66pnTt3jrsMSZoqDz744DeqanO/bRMR7jt37mR+fn7cZUjSVEnyleW2DTQtk+T5ST6Q5NEkjyT5uSRXJzme5FSzvKpn/7uSnE7yWJJbRtEJSdLgBp1z/wvg/qr6aeAlwCPAAeBEVe0CTjS3SXI9sBe4AdgN3J1k06gLlyQtb9VwT/I84FXAuwGq6rtV9S1gD3Ck2e0IcHuzvge4p6qeqKrHgdPAjaMtW5K0kkFG7i8AFoG/TfLZJO9K8lzg2qo6D9AstzT7bwPO9hy/0LQ9Q5L9SeaTzC8uLq6rE5KkZxok3K8AXg68s6peBvwPzRTMMtKn7bIvsKmqw1U1V1Vzmzf3fbNXkjSkQcJ9AVioqk81tz/AUthfSLIVoFle7Nl/R8/x24FzoylXkjSIVcO9qr4OnE3yoqbpJuBh4Biwr2nbB9zbrB8D9ia5Msl1wC7ggZFWLUla0aDXuf8m8N4kzwa+DPw6S08MR5PcAXwVeB1AVZ1McpSlJ4AngTur6qmRVy5JWtZA4V5VDwFzfTbdtMz+B4GDw5clSVoPv1tGWoOdB+4bdwnSQAx3aRUGuqaR4S5JHWS4S1IHGe6S1EGGu7QM59o1zQx3aY0MfU0Dw126RG94rxTkhrwmmeEu9bHW4DboNWkMd0nqIMNdGsLTI/VBp3CkjWa4SwMwuDVtDHfNrH6BPUyIG/yaRIa7Zt7OA/eNLKANek0Kw10akMGtaWK4Sw3DW11iuGumtRXoPlFo3Ax3zRyDV7PAcNdM2Yhg73cNvLTRDHfNJINXXWe4a2YY6Jolhrs6z1DXLDLcpRb1fkDKJxltJMNdkjrIcJekDjLcJamDBgr3JGeSfCHJQ0nmm7arkxxPcqpZXtWz/11JTid5LMktbRUvTRvn3bVR1jJy//mqemlVzTW3DwAnqmoXcKK5TZLrgb3ADcBu4O4km0ZYszSwSQlT/6iHNtp6pmX2AEea9SPA7T3t91TVE1X1OHAauHEdv0caioGqWTZouBfw0SQPJtnftF1bVecBmuWWpn0bcLbn2IWm7RmS7E8yn2R+cXFxuOolSX1dMeB+r6yqc0m2AMeTPLrCvunTVpc1VB0GDgPMzc1dtl2SNLyBRu5Vda5ZXgQ+xNI0y4UkWwGa5cVm9wVgR8/h24FzoypYkrS6VcM9yXOT/NjT68CrgS8Cx4B9zW77gHub9WPA3iRXJrkO2AU8MOrCJUnLG2Ra5lrgQ0me3v99VXV/kk8DR5PcAXwVeB1AVZ1MchR4GHgSuLOqnmqlemmKPf0m75lDt465EnXRquFeVV8GXtKn/T+Bm5Y55iBwcN3VSR3l1Ttqm59QVecYnJLhLkmdZLirUxy1S0sMd3WGn0iVfsBwlyaET0gaJcNdnTDtwehfa9KoGe6aetMeiNNevyaT4S5JHWS4S1IHGe6S1EGGuyR1kOEuSR1kuEsTyCtotF6Gu6aWASgtz3DXVDPgpf4Md2mC+eSlYRnu0oQy2LUehrumhmEnDc5w19Qx5KXVGe7ShPHJS6NguEtSB6Wqxl0Dc3NzNT8/P+4yNOFmfUR75tCt4y5BEybJg1U112+bI3dJ6iDDXZI6yHDXVJj1KRlprQx3Seogw10Tz1G7tHYDh3uSTUk+m+TDze2rkxxPcqpZXtWz711JTid5LMktbRQuSVreWkbubwYe6bl9ADhRVbuAE81tklwP7AVuAHYDdyfZNJpyJUmDGCjck2wHbgXe1dO8BzjSrB8Bbu9pv6eqnqiqx4HTwI0jqVYzxymZH/D/Qmsx6Mj9z4G3At/rabu2qs4DNMstTfs24GzPfgtN2zMk2Z9kPsn84uLiWuuWJK1g1XBP8kvAxap6cMCfmT5tl30MtqoOV9VcVc1t3rx5wB8tSRrEFQPs80rgtiSvAZ4DPC/JPwIXkmytqvNJtgIXm/0XgB09x28Hzo2yaEnSylYduVfVXVW1vap2svRG6b9W1a8Ax4B9zW77gHub9WPA3iRXJrkO2AU8MPLKJUnLWs917oeAm5OcAm5ublNVJ4GjwMPA/cCdVfXUegvV7PENxMs9/X/i/41W47dCamIZYKvzmyJnm98KKUkzxnCXpA4y3CWpgwx3SeqgQa5zlzaUb6RK6+fIXZI6yHCXppivcrQcw12acga8+jHcJamDDHdNDEeg0ugY7pLUQYa7JoKj9vXx/0+XMtw1UQwpaTQMd0nqIMNdkjrIcNfYORUzOv5f6mmGu8bKMJLaYbhLUgcZ7lJH+PdV1ctw19gYQlJ7DHdJ6iDDXZI6yHCXpA4y3KUO8v0MGe6S1EGGu9RRjt5n26rhnuQ5SR5I8rkkJ5P8QdN+dZLjSU41y6t6jrkryekkjyW5pc0OSJIuN8jI/QngF6rqJcBLgd1JXgEcAE5U1S7gRHObJNcDe4EbgN3A3Uk2tVC7ppijSqldq4Z7LflOc/NZzb8C9gBHmvYjwO3N+h7gnqp6oqoeB04DN46yaEnSygaac0+yKclDwEXgeFV9Cri2qs4DNMstze7bgLM9hy80bZf+zP1J5pPMLy4urqMLmjaO2qX2DRTuVfVUVb0U2A7cmORnVtg9/X5En595uKrmqmpu8+bNAxUrSRrMmq6WqapvAR9naS79QpKtAM3yYrPbArCj57DtwLn1FipJGtwgV8tsTvL8Zv2HgV8EHgWOAfua3fYB9zbrx4C9Sa5Mch2wC3hgxHVrSjklI22MQUbuW4GPJfk88GmW5tw/DBwCbk5yCri5uU1VnQSOAg8D9wN3VtVTbRSv6WGoSxsrVZdNh2+4ubm5mp+fH3cZapHhPl5nDt067hLUgiQPVtVcv21+QlWSOshwl6QOMtylGeHU2Gwx3CWpgwx3aQb4x7Nnj+EuSR1kuKs1jhYnk/fHbDDcJamDDHe1ylGiNB6GuyR1kOEuzSBfUXWf4S5JHWS4S1IHGe6S1EGGuyR1kOGukfPNOmn8DHdJ6iDDXa1w9D4dvJ+6y3DXSBkW0mQw3KUZ5Re7dZvhLkkdZLhLUgcZ7pKcmukgw10jY0BIk8Nwl6QOMtwlAb7y6hrDXSNhMEiTZdVwT7IjyceSPJLkZJI3N+1XJzme5FSzvKrnmLuSnE7yWJJb2uyAxs9glybPICP3J4HfqaoXA68A7kxyPXAAOFFVu4ATzW2abXuBG4DdwN1JNrVRvCSpv1XDvarOV9VnmvX/Bh4BtgF7gCPNbkeA25v1PcA9VfVEVT0OnAZuHHHdmhCO2qXJtKY59yQ7gZcBnwKurarzsPQEAGxpdtsGnO05bKFpu/Rn7U8yn2R+cXFxiNIlScsZONyT/CjwQeC3q+rbK+3ap60ua6g6XFVzVTW3efPmQcuQ1KKdB+7z1VhHDBTuSZ7FUrC/t6r+qWm+kGRrs30rcLFpXwB29By+HTg3mnI1SQwBaXINcrVMgHcDj1TVn/ZsOgbsa9b3Aff2tO9NcmWS64BdwAOjK1mStJorBtjnlcCvAl9I8lDT9nvAIeBokjuArwKvA6iqk0mOAg+zdKXNnVX11KgLlyQtb9Vwr6p/o/88OsBNyxxzEDi4jrokjdHOA/dx5tCt4y5D6+AnVCUty/dVppfhLkkdZLhrKI7opMlmuEtSBw1ytYz0fY7YZ4f39XRz5C5JHWS4S1IHGe4amC/TZ5P3+3Qy3DUQH+Czzft/+hjukgZmyE8Pw12r8gEtTR/DXdJAfJKfLoa7JHWQ4S5pTRzBTwfDXSvygSxNJ8NdkjrIcJekDjLctSynZLQcz43JZ7hLUgcZ7urLkZk03Qx3Seogw13S0HyFN7kMdz2DD1atlefMZDLcJamDDHddxpGYBtF7nnjOTB7DXd/nA1TqjlXDPcl7klxM8sWetquTHE9yqlle1bPtriSnkzyW5Ja2CpckLW+QkfvfAbsvaTsAnKiqXcCJ5jZJrgf2Ajc0x9ydZNPIqpUkDWTVcK+qTwDfvKR5D3CkWT8C3N7Tfk9VPVFVjwOngRtHU6qkSbbzwH1O7U2QYefcr62q8wDNckvTvg0427PfQtN2mST7k8wnmV9cXByyDI2CD0iNkufTZBj1G6rp01b9dqyqw1U1V1VzmzdvHnEZGpQPRLXB82r8hg33C0m2AjTLi037ArCjZ7/twLnhy9NG8cEodcuw4X4M2Nes7wPu7Wnfm+TKJNcBu4AH1leiJGmtBrkU8v3AvwMvSrKQ5A7gEHBzklPAzc1tquokcBR4GLgfuLOqnmqreEmTy1eD43XFajtU1euX2XTTMvsfBA6upyhJ0vr4CdUZ03u5miMrqbsMd0nqIMN9Rjlq10bwg03jY7hLUget+oaqusHRkzRbHLlLap2Di41nuEtSBxnukjaEb65uLMO943xAadJ4Pm4Mw72jfABpknl+ts9w7zAfQNLsMtwlqYO8zr1jHK1rWlx6rp45dOuYKukmR+6S1EGGuyR1kOEuSR3knPuUc45dUj+Gu6SJ0DtQ8c3V9XNaZko5Ype0klTVuGtgbm6u5ufnx13G1DDYNSscwa8syYNVNddvmyP3KWGgaxZ53g/PcJ8inuiaRf5B9+EY7lOg96T2BNcs8jGwds65TyhPYGllzsc75z51DHZpdf6tgpUZ7hPCE1Uajo+b/pyWGTNPTGm0zhy6lZ0H7puJaZuxTMsk2Z3ksSSnkxxo6/dMk0uD3GCXRu/SN19n9XHWysg9ySbgS8DNwALwaeD1VfVwv/27NnLvHTXM6oklTbKujOpXGrm39d0yNwKnq+rLTQH3AHuAvuE+SZZ7ObfWkDbUpcm11sdn72BtWp4Y2gr3bcDZntsLwM/27pBkP7C/ufmdJI+1VEs/1wDfWG5j3rGBlbRjxf51QNf7B93v41T1rzcTBsyHjerfTy23oa1wT5+2Z8z/VNVh4HBLv39FSeaXeynTBfZv+nW9j/avfW29oboA7Oi5vR0419LvkiRdoq1w/zSwK8l1SZ4N7AWOtfS7JEmXaGVapqqeTPIm4CPAJuA9VXWyjd81pLFMB20g+zf9ut5H+9eyifgQkyRptPz6AUnqIMNdkjpo6sM9yXuSXEzyxWW2J8lfNl+D8PkkL+/Z1vcrEpJcneR4klPN8qqN6Es/LfXvdUlOJvlekrFertVS//44yaPN/h9K8vwN6EpfLfXvD5t9H0ry0SQ/uRF96aeN/vVsf0uSSnJNm31YSUv339uTfK25/x5K8ppWiq+qqf4HvAp4OfDFZba/BvgXlq69fwXwqaZ9E/AfwAuAZwOfA65vtv0RcKBZPwC8o2P9ezHwIuDjwFwH779XA1c06+/o4P33vJ7jfwv46y71r9m+g6ULMr4CXNOl/gFvB97Sdu1TP3Kvqk8A31xhlz3A39eSTwLPT7KVnq9IqKrvAk9/RcLTxxxp1o8At7dS/ADa6F9VPVJVG/mJ4GW11L+PVtWTzfGfZOlzFmPRUv++3XP8c7nkA4IbqaXHH8CfAW9ljH2DVvvXuqkP9wH0+yqEbSu0A1xbVecBmuWWDahzWMP0b5qst3+/wdLIalIN1b8kB5OcBd4A/P4G1DmsNfcvyW3A16rqcxtV5DoMe36+qZnGeU9b076zEO7LfRXCql+RMCXs3zPbf3Bg8jbgSeC9LdQ1KkP1r6reVlU7WOrbm1qqbRTW1L8kPwK8jcl+wuo1zP33TuCFwEuB88CftFHYLIT7cl+FsNJXJFxoXlrRLC9uQJ3DGqZ/02So/iXZB/wS8IZqJjon1Hrvv/cBv9xadeu31v69ELgO+FySM037Z5L8xIZUu3Zrvv+q6kJVPVVV3wP+hqUpnJGbhXA/Bvxa8672K4D/aqZaVvqKhGPAvmZ9H3DvRhe9BsP0b5qsuX9JdgO/C9xWVf87rsIHNEz/dvUcfxvw6EYXvQZr6l9VfaGqtlTVzqrayVJIvryqvj62HqxsmPtva8/xrwX6Xomzbm2/Y9v2P+D9LL20+T+WToQ7gDcCb2y2B/grlt65/gI9V4ew9E73l5ptb+tp/3HgBHCqWV7dsf69tvlZTwAXgI90rH+nWZrvfKj5N86rSdro3wdZCoTPA/8MbOtS/y75+WcY79Uybdx//9Ds+3mWAn9rG7X79QOS1EGzMC0jSTPHcJekDjLcJamDDHdJ6iDDXZI6yHCXpA4y3CWpg/4fscuJTnjYLikAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.hist(G_rho,bins=1000,density=True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "3dbec2f1-9a2c-47e6-84aa-6801425de336",
   "metadata": {},
   "outputs": [],
   "source": [
    "rho_a=G_rho.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "6af447f2-f24f-4df9-a32c-3a9bf735236d",
   "metadata": {},
   "outputs": [],
   "source": [
    "u_rho=G_rho.std()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "7e2d7542-84f0-4ce4-b0c4-48f8d043681e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1.002524401723284, 0.0006872487357190778)"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rho_a,u_rho"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a78c3918-82db-416f-8bbb-c3377af69f42",
   "metadata": {},
   "source": [
    "Se observa que al incluir las correlaciones, la incertidumbre estándar estimada aumentó. Si se contara con una mayor cantidad de datos se podría estimar mejor la matriz de covarianzas y el resultado obtenido sería mejor."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab60b80d-2286-4134-9d54-8a5fbf1981d0",
   "metadata": {},
   "source": [
    "# Ejemplo 3: Procedimiento adaptativo\n",
    "\n",
    "El método adaptativo busca realizar $M$ ensayos un total de $h$ de veces, según se cumpla la tolerancia deseada, con $h\\geq2$. Al final del método se puede obtener un mejor estimado del mensurando de la forma:\n",
    "\n",
    "$$y=\\frac{1}{h}\\sum_{r=1}^{h}y^{r}$$\n",
    "\n",
    "Donde:\n",
    "- $y^{r}$ es el promedio de los $M$ resultados obtenidos en el ensayo $r$.\n",
    "\n",
    "Además, se debe estimar la desviación estándar como:\n",
    "\n",
    "$$s_{y}^{2}=\\frac{1}{h(h-1)}\\sum_{r=1}^{h}(y^{r}-y)^{2}$$\n",
    "\n",
    "Esto se debe calcular también para $y_{low}$, $y_{high}$ y $u(y)$, de forma que se conozcan las desviaciones estándar y valores de cada uno.\n",
    "\n",
    "Los pasos del método adaptativo son:\n",
    "\n",
    "1. Se inicia por definir la tolerancia del proceso, la recomendación propone escribir la incertidumbre estándar buscada $z$,  con $n_{dig}$ cifras signinificativas, la forma $c\\times10^{l}$, para luego escribir la tolerancia del proceso como:\n",
    "$$\\delta=\\frac{1}{2}10^{l}$$\n",
    "2. Definir $M$, este se obtiene de seleccionar el máximo entre $10^4$ y $J$, donde $J$ corresponde al número entero menor más cercano a $100/(1-p)$.\n",
    "3. Iniciar el conteo de $h$.\n",
    "4. Realizar el procedimiento regular para estimar los parámetros de interés y almacenarlos.\n",
    "5. $h+=1$ y se repite el punto 4.\n",
    "6. Cuando ya se tiene que $h>1$, entonces se estiman los valores de $y$ y $u(y)$ promedio, y sus desviaciones estándar.\n",
    "7. Si $s_{y}$ o $s_{u(y)}$ son mayores a $\\delta$, se hace $h+=1$ y se repite desde $4$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "1efc0a6f-4a9f-4ae0-821f-9a2579fd87cf",
   "metadata": {},
   "outputs": [],
   "source": [
    "# se inicializa\n",
    "h=1\n",
    "p=0.95\n",
    "M=max([10**4,100/(1-p)])\n",
    "A=np.array([[858.7,858.5],\n",
    "            [25.4,25.2],\n",
    "           [0.683,0.692]])\n",
    "np.cov(A)\n",
    "d=0.5*10**(-5)\n",
    "MuestreoMultiVariado=multivariate_normal(np.array([P,T,HR]),np.cov(A),size=M)\n",
    "Presión=MuestreoMultiVariado[:,0]+uniform(-br_p,br_p,size=M)+uniform(-be_p,be_p,size=M)+uniform(-bd_p,bd_p,size=M)+uniform(-bv_p,bv_p,size=M)\n",
    "Temperatura=MuestreoMultiVariado[:,1]+uniform(-br_t,br_t,size=M)+uniform(-be_t,be_t,size=M)+uniform(-bd_t,bd_t,size=M)+uniform(-bv_t,bv_t,size=M)\n",
    "HumedadR=MuestreoMultiVariado[:,2]+uniform(-br_hr,br_hr,size=M)+uniform(-be_hr,be_hr,size=M)+uniform(-bd_hr,bd_hr,size=M)+uniform(-bv_hr,bv_hr,size=M)\n",
    "G_rho=Densidad(Presión,Temperatura,HumedadR).tolist()\n",
    "G_rho.sort()\n",
    "G_rho=np.array(G_rho)\n",
    "rho_a=G_rho.mean()\n",
    "u_rho=G_rho.std()\n",
    "\n",
    "# se prepara la matriz hxM, y los vectores y y u(y).\n",
    "\n",
    "H=G_rho.reshape(1,M)\n",
    "y=np.array([rho_a])\n",
    "uy=np.array([u_rho])\n",
    "\n",
    "# se inician los ensayos\n",
    "\n",
    "while True:\n",
    "    MuestreoMultiVariado=multivariate_normal(np.array([P,T,HR]),np.cov(A),size=M)\n",
    "    Presión=MuestreoMultiVariado[:,0]+uniform(-br_p,br_p,size=M)+uniform(-be_p,be_p,size=M)+uniform(-bd_p,bd_p,size=M)+uniform(-bv_p,bv_p,size=M)\n",
    "    Temperatura=MuestreoMultiVariado[:,1]+uniform(-br_t,br_t,size=M)+uniform(-be_t,be_t,size=M)+uniform(-bd_t,bd_t,size=M)+uniform(-bv_t,bv_t,size=M)\n",
    "    HumedadR=MuestreoMultiVariado[:,2]+uniform(-br_hr,br_hr,size=M)+uniform(-be_hr,be_hr,size=M)+uniform(-bd_hr,bd_hr,size=M)+uniform(-bv_hr,bv_hr,size=M)\n",
    "    G_rho=Densidad(Presión,Temperatura,HumedadR).tolist()\n",
    "    G_rho.sort()\n",
    "    G_rho=np.array(G_rho)\n",
    "    rho_a=G_rho.mean()\n",
    "    u_rho=G_rho.std()\n",
    "    H=np.append(H,G_rho.reshape(1,M),axis=0)\n",
    "    y=np.append(y,rho_a)\n",
    "    uy=np.append(uy,u_rho)\n",
    "    \n",
    "    h+=1\n",
    "    \n",
    "    sy=np.sqrt(1/(h*(h-1))*((y.mean()-y)**2).sum())\n",
    "    suy=np.sqrt(1/(h*(h-1))*((uy.mean()-uy)**2).sum())\n",
    "    \n",
    "    if 2*sy<d or 2*suy<d: # para si se cumple el criterio de tolerancia\n",
    "        break\n",
    "    else:\n",
    "        pass # continua si no\n",
    "valor=H.mean() # el mejor estimador de y es el promedio de la matriz hxM\n",
    "incertidumbre=H.std() # la incertidumbre estándar viene de la matriz hxM también"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "c99b5b9c-9124-4fa3-9265-1b30bbf60a81",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1.002525127211968, 0.0006872469961063696, 2)"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "valor,incertidumbre,h"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
